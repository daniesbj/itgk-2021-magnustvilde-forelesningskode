'''eksempel - vi setter sammen det som ble gjennomgått forrige uke'''

# Oppgave:
'''
1. Hent inn navnet til bruker. Hent inn tre forskjellige tall fra bruker. De to første
    skal være heltall (integer), det siste skal være et flyttall (float).

2. Lag en variabel der de to heltallene tallene blir addert,
    og så delt på det siste tallet.

3. Lagre tallet som kommer på tre forskjellige former (f, %, e).

4. Print ut igjen en beskrivende tekst til brukeren. Teksten skal inneholde det opprinnelige
    tallet, samt hva som har blitt gjort med tallet for å få det gjort om til de tre
    forskjellige formene.
'''

# 1
navn = input('Skriv navnet ditt: ')
tall1 = input('Skriv inn et heltall: ')
tall1 = int(tall1)
tall2 = int(input('Skriv inn enda et heltall: '))
flyt = float(input('Skriv inn et flyttall: '))

# 2
resultat = (tall1+tall2)/flyt

# 3: f, %, e
flyt = f'{resultat:.1f}'
prosent = f'{resultat:.0%}'
vitenskap = f'{resultat:e}'

#4
'''4. Print ut igjen en beskrivende tekst til brukeren. Teksten skal inneholde det opprinnelige
    tallet, samt hva som har blitt gjort med tallet for å få det gjort om til de tre
    forskjellige formene.'''

print(f'Se her {navn}! Nå har programmet hentet inn tre tall, ')
print('lagt sammen de to første, og delt resultatet på det siste flyttallet.')
print(f'   Avrundet:    {flyt}')
print(f'   Prosent:    {prosent}')
print(f'   Vitenskapelig notasjon:    {vitenskap}')